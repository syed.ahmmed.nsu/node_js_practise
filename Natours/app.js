const express = require('express');
const morgan = require('morgan');

const AppError = require('./utils/appError');
const globalErrorHandler = require('./controllers/errorController');
const tourRouter = require('./routes/tourRoutes');
const userRouter = require('./routes/userRoutes');

const app = express();

//1.> middleware

app.use(morgan('tiny'));

app.use(express.json());

app.use(express.static(`${__dirname}/public`));

app.use((req, res, next) => {
  console.log('Hello from the middleware 🙋‍♂️');
  next();
});

// app.use((req, res, next) => {
//   req.requestTime = new Date().toISOString();
//   next();
// });

// app.get('/', (req, res) => {
//1a.> send inf. to server
//   res
//     .status(200)
//     .send('Hello from the sever side');
//1b...> now json data

//   res.status(200).json({
//     message: 'Hello from the Server side',
//     app: 'Natours',
//   });

//1c.> if we put in (status) -- 404
//   res.status(404).json({
//     message: 'Hello from the Server side',
//     app: 'Natours',
//   });
// });

//2a.> HTTP POST method

// app.post('/', (req, res) => {
//   res.status(200).send('This is a post method');
// });

// Tours controller

//User controller

// Route

// app.get('/api/v1/tours', getAllTour);

// app.get(, getTour);

// app.post('/api/v1/tours', createTour);

// app.delete('/api/v1/tours/:id', deleteTour);

// app.patch('/api/v1/tours/:id', updateTour);

app.use('/api/v1/tours', tourRouter);
app.use('/api/v1/users', userRouter);

app.all('*', (req, res, next) => {
  // res.status(404).json({
  //   status: 'fail',
  //   message: `can't find ${req.originalUrl} on this server!`,
  // });

  // const err = new Error(
  //   `can't find ${req.originalUrl} on this server!`
  // );

  // err.statusCode = 404;
  // err.status = 'fail';
  // next(err);

  next(
    new AppError(
      `can't find ${req.originalUrl} on this server!`,
      404
    )
  );
});

app.use(globalErrorHandler);

module.exports = app;
