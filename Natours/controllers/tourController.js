// const fs = require('fs');
const Tour = require('./../models/tourModel');
const APIFeatures = require('./../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');
const appError = require('./../utils/appError');
const AppError = require('./../utils/appError');

exports.aliasTopTours = (req, res, next) => {
  req.query.limit = '5';
  req.query.sort = '-ratingsAverage,price';
  req.query.fields =
    'name,price,ratingsAverage,summary,difficulty';
  next();
};

// class APIFeatures {
//   constructor(query, queryString) {
//     this.query = query;
//     this.queryString = queryString;
//   }

//   filter() {
//     const queryObj = { ...queryString };
//     const excludeFields = [
//       'page',
//       'sort',
//       'limit',
//       'fields',
//     ];
//     excludeFields.forEach(
//       (el) => delete queryObj[el]
//     );

//     //2B .> advanced filtering
//     let queryStr = JSON.stringify(queryObj);
//     queryStr = queryStr.replace(
//       /\b(gte|gt|lte|lt)\b/g,
//       (match) => `$${match}`
//     );
//     this.query = Tour.find(JSON.parse(queryStr));

//     return this;
//   }

//   sort() {
//     if (req.queryString.sort) {
//       const sortBy = req.query.sort
//         .split(',')
//         .join(' ');
//       console.log(sortBy);
//       this.query = this.query.sort(sortBy);
//     } else {
//       this.query = this.query.sort('-createdAt');
//     }

//     return this;
//   }

//   limitFields() {
//     if (req.queryStirng.fields) {
//       const fields = req.queryStirng.fields
//         .split(',')
//         .join(' ');

//       this.query = this.query.select(fields);
//     } else {
//       this.query = this.query.select('-__v');
//     }
//     return this;
//   }

//   paginate() {
//     const page = this.queryString.page * 1 || 1;
//     const limit =
//       this.queryString.limit * 1 || 100;
//     const skip = (page - 1) * limit;

//     this.query = this.query
//       .skip(skip)
//       .limit(limit);

//     // if (this.queryString.page) {
//     //   const numTours = await Tour.countDocuments();
//     //   if (skip >= numTours)
//     //     throw new Error(
//     //       'This page does not exits'
//     //     );
//     return this;
//   }
// }

// const tours = JSON.parse(
//   fs.readFileSync(
//     `${__dirname}/../dev-data/data/tours-simple.json`
//   )
// );

// exports.checkID = (req, res, next, val) => {
//   console.log(`Tour id is: ${val}`);

//   if (req.params.id * 1 > tours.length) {
//     return res.status(404).json({
//       status: 'fail',
//       message: 'Invalid ID',
//     });
//   }
//   next();
// };

// exports.checkBody = (req, res, nextl) => {
//   if (!req.body.name || !req.body.price) {
//     return res.status(400).json({
//       status: 'fail',
//       message: 'Missing name or price',
//     });
//   }
// };
// const queryObj = { ...req.query };
// const excludeFields = [
//   'page',
//   'sort',
//   'limit',
//   'fields',
// ];
// excludeFields.forEach(
//   (el) => delete queryObj[el]
// );

// //2B .> advanced filtering
// let queryStr = JSON.stringify(queryObj);
// queryStr = queryStr.replace(
//   /\b(gte|gt|lte|lt)\b/g,
//   (match) => `$${match}`
// );
// let query = Tour.find(JSON.parse(queryStr));

// const tours = await Tour.find(
//   JSON.parse(queryStr)
// );
//SORTING
// if (req.query.sort) {
//   const sortBy = req.query.sort
//     .split(',')
//     .join(' ');
//   console.log(sortBy);
//   query = query.sort(sortBy);
// } else {
//   query = query.sort('-createdAt');
// }

//LIMIT Fields

// if (req.query.fields) {
//   const fields = req.query.fields
//     .split(',')
//     .join(' ');

//   query = query.select(fields);
// } else {
//   query = query.select('-__v');
// }

// const tours = await Tour.find()
//   .where('duration')
//   .equals(5)
//   .where('difficulty')
//   .equals('easy');

//Paginaton
// const page = req.query.page * 1 || 1;
// const limit = req.query.limit * 1 || 100;
// const skip = (page - 1) * limit;

// query = query.skip(skip).limit(limit);

// if (req.query.page) {
//   const numTours = await Tour.countDocuments();
//   if (skip >= numTours)
//     throw new Error(
//       'This page does not exits'
//     );
// }
exports.getAllTours = catchAsync(
  async (req, res) => {
    //Execute Query
    const features = new APIFeatures(
      Tour.find(),
      req.query
    )
      .filter()
      .sort()
      .limitFields()
      .paginate();

    const tours = await features.query;

    res.status(200).json({
      status: 'success',
      result: tours.length,

      data: { tours },
    });
    // try {

    // } catch (err) {
    //   res.status(404).json({
    //     status: 'Fail',
    //     message: err,
    //   });
    // }
  }
);

// exports.getAllTours = (req, res) => {
//   console.log(req.requestTime);

//   res.status(200).json({
//     status: 'success',
//     requestedAt: req.requestTime,

//     data: { tours },
//   });
// };

// exports.getTour = (req, res) => {
//   const id = req.params.id * 1;

//   if (id > tours.length) {
//     return res.status(404).json({
//       status: 'fail',
//       message: ' Invalid ID',
//     });
//   }

//   const tour = tours.find((el) => el.id === id);

//   res.status(200).json({
//     status: 'success',
//     data: {
//       tour,
//     },
//   });
// };

exports.getTour = catchAsync(
  async (req, res, next) => {
    const tour = await Tour.findById(
      req.params.id
    );

    if (!tour) {
      return next(
        new AppError(
          'the page not found with that id',
          404
        )
      );
    }

    res.status(200).json({
      status: 'success',
      data: {
        tour,
      },
    });
    // try {

    // } catch (err) {
    //   res.status(404).json({
    //     status: 'fail',
    //     message: ' Invalid ID',
    //   });
    // }
  }
);

exports.createTour = catchAsync(
  async (req, res) => {
    // console.log(req.body);

    const newTour = await Tour.create(req.body);

    res.status(201).json({
      status: 'success',
      data: {
        tour: newTour,
      },
    });

    // try {

    // } catch (err) {
    //   res.status(400).json({
    //     status: 'fail',
    //     message: err,
    //   });
    // }
  }
);

// exports.createTour = (req, res) => {
//   // console.log(req.body);

//   const newId = tours[tours.length - 1].id + 1;
//   const newTour = Object.assign(
//     { id: newId },
//     req.body
//   );

//   tours.push(newTour);

//   fs.writeFile(
//     `${__dirname}/dev-data/data/tours-simple.json`,
//     JSON.stringify(tours),
//     (err) => {
//       res.status(201).json({
//         status: 'success',
//         data: {
//           tour: newTour,
//         },
//       });
//     }
//   );
// };

exports.updateTour = catchAsync(
  async (req, res) => {
    const tour = await Tour.findByIdAndUpdate(
      req.params.id,
      req.body,
      {
        runValidators: true,
      }
    );

    res.status(200).json({
      status: 'success',
      data: {
        tour,
      },
    });
    // try {

    // } catch (err) {
    //   res.status(404).json({
    //     status: 'fail',
    //     message: ' Invalid ID',
    //   });
    // }
  }
);

// exports.updateTour = (req, res) => {
//   const id = req.params.id * 1;

//   if (id > tours.length) {
//     return res.status(404).json({
//       status: 'fail',
//       message: ' Invalid ID',
//     });
//   }

//   res.status(200).json({
//     status: 'success',
//     data: {
//       tour: '<Updated  here ....>',
//     },
//   });
// };
exports.deleteTour = catchAsync(
  async (req, res, next) => {
    const tour = await Tour.findByIdAndDelete(
      req.params.id
    );

    if (!id) {
      return new Error(
        'can not find the page with this id',
        404
      );
    }

    res.status(200).json({
      status: 'success',
      data: null,
    });
  }
);

exports.getTourStates = catchAsync(
  async (req, res) => {
    const stats = await Tour.aggregate([
      {
        $match: {
          ratingsAverage: { $gte: 4.5 },
        },
      },
      {
        $group: {
          _id: { $toUpper: '$difficulty' },
          numTours: { $sum: 1 },
          numRatings: {
            $sum: '$ratingsQuantity',
          },
          avgRating: {
            $avg: '$ratingsAverage',
          },
          avgPrice: { $avg: '$price' },
          minPrice: { $min: '$price' },
          maxPrice: { $max: '$price' },
        },
      },
      {
        $sort: { avgPrice: 1 },
      },
    ]);
    res.status(200).json({
      status: 'success',
      data: {
        stats,
      },
    });
    // try {

    // } catch (err) {
    //   res.status(404).json({
    //     status: 'fail',
    //     message: err,
    //   });
    // }
  }
);

exports.monthlyPlan = catchAsync(
  async (req, res) => {
    const year = req.params.year * 1;

    const plan = await Tour.aggregate([
      {
        $unwind: '$startDates',
      },
      {
        $match: {
          startDates: {
            $gte: new Date(`${year}-01-01`),
            $lte: new Date(`${year}-12-01`),
          },
        },
      },
      {
        $group: {
          _id: { $month: '$startDates' },
          numTourStart: { $sum: 1 },
          tours: { $push: '$name' },
        },
      },
      {
        $addFields: { month: '$_id' },
      },
      {
        $project: {
          _id: 0,
        },
      },
      {
        $sort: { numTourStart: -1 },
      },
      {
        $limit: 6,
      },
    ]);

    res.status(200).json({
      status: 'Success',
      data: {
        plan,
      },
    });
    // try {
    //   ;
    // } catch (err) {
    //   res.status(404).json({
    //     status: 'fail',
    //     message: err,
    //   });
  }
);
