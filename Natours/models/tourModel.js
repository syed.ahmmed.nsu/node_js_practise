const mongoose = require('mongoose');
const slugify = require('slugify');

const tourSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'A tour must have a name'],
      unique: true,
      trim: true,
      maxlength: [
        40,
        'A tour name must not have many than 40 char',
      ],
      minlength: [
        10,
        'A tour name must not have less than 10 char',
      ],
    },
    duration: {
      type: Number,
      required: [
        true,
        'A tour must have a duration',
      ],
    },

    maxGroupSize: {
      type: Number,
      required: [
        true,
        'A tour must have a group size',
      ],
    },
    difficulty: {
      type: String,
      required: [
        true,
        'A tour must have a price',
      ],
      enum: {
        values: ['easy', 'medium', 'difficult'],
        message:
          'difficult either be: easy  medium and difficult',
      },
    },

    ratingsAverage: {
      type: Number,
      default: 4.5,
      min: [1, 'Rating must be above 1.0'],
      max: [5, 'Rating must be below 5.0'],
    },

    ratingsQuantity: {
      type: Number,
      default: 0,
    },

    price: {
      type: Number,
      required: [
        true,
        'A tour must have a price',
      ],
    },
    priceDiscount: Number,
    summary: {
      type: String,
      trim: true,
    },

    slug: String, //for middleware tourSchema.pre ,
    description: {
      type: String,
      trim: true,
    },
    imageCover: {
      type: String,
      request: [
        true,
        'A tour must have a cover image',
      ],
    },
    images: [String],
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    startDates: [Date],
    secretTour: {
      type: Boolean,
      default: false,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

tourSchema
  .virtual('durationWeeks')
  .get(function () {
    return this.duration / 7;
  });

//Documents middleware , run before .save and .create , but not before (.inserMany)
tourSchema.pre('save', function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

//Query MiddleWare
tourSchema.pre(/^find/, function (next) {
  this.find({ secretTour: { $ne: true } });
  this.start = Date.now();
  next();
});

tourSchema.post(/^find/, function (doc, next) {
  console.log(
    `The time elapsed ${
      Date.now() - this.start
    } miliseconds`
  );

  next();
});
//Aggregate Middleware
tourSchema.pre('aggregate', function (next) {
  this.pipeline().unshift({
    $match: { secretTour: { $ne: true } },
  });
  console.log(this.pipeline());
  next();
});
// tourSchema.pre('save', function (next) {
//   console.log('will be save');
//   next();
// });

// tourSchema.post('save', function (doc, next) {
//   console.log(doc);
//   next();
// });

const Tour = mongoose.model('Tour', tourSchema);

module.exports = Tour;
