const fs = require('fs');
const mongoose = require('mongoose');
require('dotenv').config();
const Tour = require('./../../models/tourModel');

const DB = process.env.DATABASE_LOCAL;
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('DB connection successful!');
  });

//Read Json file
const tours = JSON.parse(
  fs.readFileSync(
    `${__dirname}/tours-simple.json`,
    'utf-8'
  )
);

//Import Data into DB
const importData = async () => {
  try {
    await Tour.create(tours);
    console.log('Data Succesfully loaded');
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

//Delete All data from DB

const deleteData = async () => {
  try {
    await Tour.deleteMany();
    console.log('Data deleted successfully!');
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

if (process.argv[2] === '--import') {
  importData();
} else if (process.argv[2] === '--delete') {
  deleteData();
}
