const mongoose = require('mongoose');
require('dotenv').config();
const app = require('./app');

console.log(process.env.NODE_ENV);

const DB = process.env.DATABASE_LOCAL;
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('DB connection successful!');
  });

// const testTour = new Tour({
//   name: 'The Forest Hiker',
//   rating: '4.7',
//   price: 497,
// });

// const testTour2 = new Tour({
//   name: 'The Fantasy Kingdom',
//   rating: '4.6',
//   price: 250,
// });

// testTour
//   .save()
//   .then((doc) => {
//     console.log(doc);
//   })
//   .catch((err) => {
//     console.log('Error : ', err);
//   });

// testTour2
//   .save()
//   .then((doc) => {
//     console.log(doc);
//   })
//   .catch((err) => {
//     console.log('Error:', err);
//   });

const port = 3000;

app.listen(port, () => {
  console.log(`Apps running on port ${port}...`);
});
