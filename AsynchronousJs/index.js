const fs = require('fs');
const { connected } = require('process');
const superagent = require('superagent');

const readFilePro = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) reject('File could not be found');
      resolve(data);
    });
  });
};

const writeFilePro = (file, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, (err) => {
      if (err) reject('File could not written');
      resolve('success');
    });
  });
};

//Consuming promise with Async Await

try {
  const gotDogPic = async () => {
    const data = await readFilePro(`${__dirname}/dog.txt`);
    console.log(`Breed : ${data}`);

    const res = await superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );

    console.log(res.body.message);

    await writeFilePro('dog-image.txt', res.body.message);
  };
} catch (err) {
  console.log(err);
}

gotDogPic();

/* readFilePro(`${__dirname}/dog.txt`)
  .then((data) => {
    console.log(`Breed : ${data}`);

    return superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );
  })
  .then((res) => {
    console.log(res.body.message);

    return writeFilePro('dogaa-image.txt', res.body.message);

    //  fs.writeFile('dog-image.txt', res.body.message, (err) => {
    //   if (err) console.log(err.message);
    //   console.log('the image file have been saved perfectly');
    // }); 
  })
  .then(() => {
    console.log('Random images save to the file');
  })
  .catch((err) => {
    console.log(err);
  }); 
  */

/* fs.readFile(`${__dirname}/dog.txt`, (err, data) => {
  console.log(`Breed: ${data}`);

  superagent
    .get(`https://dog.ceo/api/breed/${data}/images/random`)
    .then((res) => {
      console.log(res.body.message);

      fs.writeFile('dog-image.txt', res.body.message, (err) => {
        if (err) console.log(err.message);
        console.log('the image file have been saved perfectly');
      });
    })
    .catch((err) => {
      console.log(err.message);
    });
}); */
